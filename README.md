# manim 3b1b community addition

### Warning

`Always run in elevated conda-powershell with admin rights on windows.`

## Installation

### Step 1 : Building the Conda environment

After adding any necessary dependencies that should be downloaded via `conda` to the 
`environment.yml` file and any dependencies that should be downloaded via `pip` to the 
`requirements.txt` file you create the Conda environment in a sub-directory `./env`of your project 
directory by running the following commands.

```bash
export ENV_PREFIX=$PWD/env
conda env create --prefix $ENV_PREFIX --file environment.yml --force
```

Once the new environment has been created you can activate the environment with the following 
command.

```bash
conda activate $ENV_PREFIX
```

### Step 2 : Additional dependency

## Windows:<br>
You can install additional dependecies via choco. Installation instructions of choco are available on https://chocolatey.org/install. After installation execute
```pwershell
choco install miktex.install
choco install ffmpeg
```
## Linux:<br>
```bash
sudo apt install libcairo2-dev libpango1.0-dev ffmpeg
sudo apt install texlive-full
```


