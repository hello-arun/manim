After `activating the environment`, to export the animation. Run this in `elevated` conda-powershell with `admin` rights.

To export in 4k quality and then play<br>
Windows:<br>
```conda powershell
manim.exe -qk ./example.py PointMovingOnShapes
```